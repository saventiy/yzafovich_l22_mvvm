package com.example.saventiy_l22_mvvm.repository

import androidx.lifecycle.MutableLiveData
import com.example.saventiy_l22_mvvm.data.User
import com.example.saventiy_l22_mvvm.util.RandomUserGenerator

class MainRepository {

    private val usersLiveData = MutableLiveData<MutableList<User>>()
    private val usersList = mutableListOf<User>()

    fun getAll() = usersLiveData

    fun update() {
        usersList.add(RandomUserGenerator().createUser())
        usersLiveData.setValue(usersList)
    }
}