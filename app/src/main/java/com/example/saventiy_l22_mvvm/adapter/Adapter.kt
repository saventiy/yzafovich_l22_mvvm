package com.example.saventiy_l22_mvvm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.saventiy_l22_mvvm.data.User
import com.example.saventiy_l22_mvvm.R
import kotlinx.android.synthetic.main.item_view.view.*

class Adapter: RecyclerView.Adapter<Adapter.ViewHolder>() {

    var usersList: List<User> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = usersList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = usersList[position].name
    }

    fun newList(newList: List<User>){
        usersList = newList
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val name = itemView.text_view_name
    }
}