package com.example.saventiy_l22_mvvm.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.saventiy_l22_mvvm.repository.MainRepository

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = MainRepository()
    fun getAll() = repository.getAll()
    fun update() {
        repository.update()
    }

}