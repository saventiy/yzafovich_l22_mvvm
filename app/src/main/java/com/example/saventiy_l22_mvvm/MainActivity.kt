package com.example.saventiy_l22_mvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.saventiy_l22_mvvm.adapter.Adapter
import com.example.saventiy_l22_mvvm.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = Adapter()
        recycler_view_users.layoutManager = LinearLayoutManager(applicationContext)
        recycler_view_users.adapter = adapter

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.getAll().observe(this, Observer {
            adapter.newList(it)
            adapter.notifyDataSetChanged()
        })

        floatingActionButton.setOnClickListener {
            viewModel.update()
        }



    }
}
