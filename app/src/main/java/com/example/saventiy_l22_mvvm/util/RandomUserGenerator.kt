package com.example.saventiy_l22_mvvm.util

import com.example.saventiy_l22_mvvm.data.User
import kotlin.random.Random

class RandomUserGenerator {

    private var listUsersName = listOf(
        "Bernard Curtis", "Henrietta Gordon", "Marguerite Hudson",
        "Arlene Douglas", "Terrance Love", "Marjorie Guerrero",
        "Vivian Simmons", "Jodi Baker", "Nina Harris"
    )

    fun createUser() = User(getName())

    private fun getName() = listUsersName[Random.nextInt(listUsersName.size)]

}